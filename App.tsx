import {SafeAreaProvider} from 'react-native-safe-area-context';
import RootNavigation from './src/routes/index';
import {QueryClient, QueryClientProvider} from 'react-query';
import { PaperProvider } from 'react-native-paper';

const queryClient = new QueryClient();

function App(): React.JSX.Element {
  return (
    <SafeAreaProvider>
      {/* <PaperProvider> */}
        <QueryClientProvider client={queryClient}>
          <RootNavigation />
        </QueryClientProvider>
      {/* </PaperProvider> */}
    </SafeAreaProvider>
  );
}

export default App;
