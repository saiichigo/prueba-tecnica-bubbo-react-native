import { CommonActions, useNavigation } from '@react-navigation/native';

import { useEffect, useState } from 'react';
import { deleteBoook, getBookById, getBooks, postNewBook, putBook } from '../api/HttpRequets';
import { BooksResponse } from '../interfaces/BooksResponse';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { stylesHome } from '../styles/styleHome';



export const useGlobal = () => {

    const queryClient = useQueryClient();
    const navigation = useNavigation();

    const styles = stylesHome();

    const { data, error, isLoading } = useQuery<BooksResponse[], Error>({ queryKey: ['getBooks'], queryFn: getBooks });

    const redirectScreen = (route: string, id?: string) => {
        // guardamos el ID del libro
        if (id) {
            queryClient.setQueryData<string>('id_book', id)
        }

        navigation.dispatch(CommonActions.navigate(route));
    };

    const getIdToken = (): string => {
        const id = queryClient.getQueryData('id_book') as string
        return id;

    }
    const _getBookById = (id: string) => {
        return useQuery<BooksResponse, Error>(['getBook', id], () => getBookById(id), { staleTime: Infinity, enabled: !!id });
    }



    return {
        navigation,
        redirectScreen,
        data, error, isLoading,
        queryClient,
        getIdToken,
        _getBookById,
        styles
        // _deleteBook

    }
}
