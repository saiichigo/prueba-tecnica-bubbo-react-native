import React, { useState } from 'react'
import { useForm } from 'react-hook-form';
import { useGlobal } from './useGlobal';
import { useMutation } from 'react-query';
import { postNewBook } from '../api/HttpRequets';
import { Alert } from 'react-native';
import { styleNewBook } from '../styles/styleNewBooks';

export const useNewBooks = () => {
    const [date, setDate] = useState(new Date());
    const [open, setOpen] = useState(false);
    const [selectedImage, setSelectedImage] = useState('');

    const styles = styleNewBook()
    const [selectedImageBase64, setSelectedImageBase64] = useState('');
    const {
        control,
        handleSubmit,
        formState: { errors, isValid },
    } = useForm();
    const { queryClient, navigation } = useGlobal();

    const { isError, isLoading, mutate } = useMutation({
        mutationKey: ['post_newBook'],
        mutationFn: postNewBook,
    });
    const showAlert = () => {
        Alert.alert(
            'Libro creado ', // Título del Alert
            'El libro ha sido creado exitosamente.', // Mensaje del Alert
            [
                {
                    text: 'Continuar',
                    onPress: () => {
                        navigation.goBack();
                    }, // Acción al presionar
                },
            ],
        );
    };

    const onSubmit = (data: any) => {
        const newBook = {
            ...data,
            images: selectedImageBase64,
        };

        mutate(newBook, {
            onSuccess: data => {
                queryClient.invalidateQueries('getBooks');
                showAlert();
                console.log('Libro agregado con éxito:', data);
            },
            onError: error => {
                console.error('Error al agregar el libro:', error);
            },
        });
    };
    return {
        isLoading,
        selectedImage,
        setSelectedImage,
        setSelectedImageBase64,
        control,
        errors,
        setOpen,
        open,
        date,
        setDate,
        handleSubmit,
        isValid,
        onSubmit,
        styles,

    }
}
