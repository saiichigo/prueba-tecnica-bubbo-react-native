import React from 'react'
import { useGlobal } from './useGlobal';
import { useMutation } from 'react-query';
import { deleteBoook } from '../api/HttpRequets';
import { styleDetailBook } from '../styles/styleDetailBook';

export const useDetailBook = () => {
    const { _getBookById, getIdToken, redirectScreen, queryClient, navigation } =
        useGlobal();
    const styles = styleDetailBook()
    const id = getIdToken();
    const { data, error, isLoading } = _getBookById(id);
    const { isLoading: isLoadingDelete, mutate: mutateDelete } = useMutation({
        mutationKey: ['delete_newBook'],
        mutationFn: deleteBoook,
    });

    const _deleteBook = () => {
        mutateDelete(id, {
            onSuccess: data => {
                queryClient.invalidateQueries('getBooks');

                navigation.goBack();
            },
            onError: error => {
                console.error('Error al borrar el libro:', error);
            },
        });
    };

    return {
        _deleteBook,
        data,
        error,
        isLoading,
        isLoadingDelete,
        redirectScreen,
        styles
    }
}
