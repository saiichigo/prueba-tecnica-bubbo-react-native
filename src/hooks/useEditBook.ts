import React, { useState } from 'react'
import { useGlobal } from './useGlobal';
import { useForm } from 'react-hook-form';
import { useMutation } from 'react-query';
import { putBook } from '../api/HttpRequets';
import { Alert } from 'react-native';
import { styleEditBoook } from '../styles/styleEditBooks';

export const useEditBook = () => {
    const { _getBookById, getIdToken, queryClient, navigation } = useGlobal();
    const styles = styleEditBoook()
    const id = getIdToken();

    //   aqui no se lanza de nuevo la peticción porque los datos ya estan en cache.
    const { data, isLoading } = _getBookById(id);
    const {
        control,
        handleSubmit,
        setValue,
        formState: { errors, isValid },
    } = useForm();
    const [date, setDate] = useState(new Date());
    const [open, setOpen] = useState(false);
    const [selectedImage, setSelectedImage] = useState('');
    const [selectedImageBase64, setSelectedImageBase64] = useState('');

    const { isLoading: isLoadingUpdate, mutate } = useMutation({
        mutationKey: ['update_books'],
        mutationFn: putBook,
    });

    const onSubmit = (data: any) => {
        const newBook = {
            id,
            ...data,
            images: selectedImageBase64 !== '' ? selectedImageBase64 : data.images,
        };

        mutate(newBook, {
            onSuccess: () => {
                queryClient.invalidateQueries(['getBooks']);
                queryClient.invalidateQueries(['getBook']);

                showAlert();
            },
            onError: () => { },
        });
        console.log(selectedImage);
    };

    const showAlert = () => {
        Alert.alert(
            'Libro actualizado',
            'El libro ha sido actualizado exitosamente.',
            [
                {
                    text: 'Continuar',
                    onPress: () => {
                        navigation.goBack();
                    },
                },
            ],
        );
    };

    return {
        data,
        styles,
        isLoading,
        control,
        handleSubmit,
        date,
        setDate,
        open,
        setOpen,
        setSelectedImage,
        setSelectedImageBase64,
        isLoadingUpdate,
        errors,
        onSubmit,
        isValid

    }
}
