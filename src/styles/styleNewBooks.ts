import { StyleSheet } from "react-native";

export const styleNewBook = () =>
StyleSheet.create({
    container: {
      flex: 1,
      marginHorizontal: 10,
      backgroundColor: 'white',
    },
    imageSelectorContainer: {
      flex: 0.35,
    },
    formContainer: {
      flex: 0.65,
    },
    rateText: {
      textAlign: 'center',
      fontWeight: '700',
      marginTop: 10,
      color: 'black',
    },
    submitButton: {
      justifyContent: 'center',
      borderRadius: 16,
      marginVertical: 20,
      height: 35,
    },
    submitButtonText: {
      color: 'white',
      fontWeight: '700',
    },
    textInput: {
      marginBottom: 5,
    },
    containerStyle: {
      backgroundColor: 'white',
      padding: 20,
      margin: 20,
      borderRadius: 16,
    },
  
    // Añade aquí cualquier otro estilo que necesites
  });
  