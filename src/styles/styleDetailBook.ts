import { StyleSheet } from "react-native";

export const styleDetailBook = () =>
  StyleSheet.create({
    container: {
      flex: 1,
      marginHorizontal: 10,
      backgroundColor: 'white',
    },
    imageSection: {
      flex: 0.3,
      alignItems: 'center',
      marginTop: 5,
    },
    imageBackground: {
      height: '100%',
      width: 180,
    },
    detailsSection: {
      flex: 0.3,
      // justifyContent: 'center',
      alignItems: 'center',
      marginHorizontal: 10,
      // backgroundColor: 'grey',
    },
    titleTitle: {
      fontSize: 28,
      fontWeight: 'bold',
      textAlign: 'center',
      color: 'black',
    },
    titleAuthor: {
      fontSize: 20,
      // fontWeight: 'bold',
      textAlign: 'center',
      color: 'black',
    },
    titleYear: {
      fontSize: 20,
      // fontWeight: 'bold',
      textAlign: 'center',
      color: 'black',
    },

    titleText: {
      fontSize: 16,
      color: 'grey',

      // fontWeight: 'bold',
      textAlign: 'center',
    },
    titleValue: {
      fontSize: 16,
      color: 'black',

      fontWeight: 'bold',
      textAlign: 'center',
      // color: 'black',
    },
    titleOverview: {
      fontSize: 16,
      color: 'grey',
      textAlign: 'justify',
      marginBottom: 10,

      // fontWeight: 'bold',
      // textAlign: 'center',
      // color: 'black',
    },
    infoBox: {
      flexDirection: 'row',
      backgroundColor: '#e9efe0',
      borderRadius: 16,
      height: 50,
      justifyContent: 'center',
      alignItems: 'center',
    },
    infoSection: {
      width: '33%',
      alignItems: 'center',
      borderRightColor: 'black',
      borderRightWidth: 2,
    },
    lastInfoSection: {
      width: '33%',
      alignItems: 'center',
    },
    descriptionSection: {
      flex: 0.4,
      marginHorizontal: 10,
    },
    descriptionText: {
      fontSize: 24,
      fontWeight: 'bold',
      color: 'black',
    },
  });
