import {StyleSheet} from 'react-native';

export const stylesHome = () =>
  StyleSheet.create({
    itemContainer: {
      flexDirection: 'row',
      marginVertical: 15,
      marginHorizontal: 15,
      minHeight: 200,
    },
    sectionleft: {
      width: '30%',
      borderRadius: 16,
    },
    sectionRight: {
      width: '70%',
      paddingVertical: 5,
      justifyContent: 'space-around',
      alignItems: 'flex-start',
      paddingLeft: 20,
    },

    image: {
      flex: 1,
      borderRadius: 16,
    },
    textTitle: {
      color: 'black',
      fontSize: 24,
      fontWeight: 'bold',

    },
    textAuthor: {
      color: 'black',
    },
  });
