
export interface BooksResponse {
    images:          string;
    pages?:          number;
    cost?:           number;
    rate?:           number;
    author:          string;
    genre:           string;
    publicationYear: number;
    description:     string;
    id?:             string;
    title:           string;
}

