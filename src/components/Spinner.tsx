import React from 'react';
import {ActivityIndicator, Text, View} from 'react-native';

interface Props {
  message?: string;
}

export const Spinner = ({message}: Props) => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <ActivityIndicator size="large" color="#0000ff" />
      <Text>{message}</Text>
    </View>
  );
};
