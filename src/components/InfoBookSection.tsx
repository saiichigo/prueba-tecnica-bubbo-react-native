import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {useDetailBook} from '../hooks/useDetailBook';
import {BooksResponse} from '../interfaces/BooksResponse';

interface Props {
  data: string | number;
  title: string,
  disableLine? : boolean
}

const InfoBookSection = ({data,title, disableLine = false}: Props) => {
  const {styles} = useDetailBook();
  return (

  <>
  {
    !disableLine ? ( <View style={styles.infoSection}>
      <Text style={styles.titleText}>{title}</Text>
      <Text style={styles.titleValue}>{data}</Text>
    </View>) : (
       <View style={{...styles.infoSection, borderRightWidth: 0}}>
       <Text style={styles.titleText}>{title}</Text>
       <Text style={styles.titleValue}>{data}</Text>
     </View>
    )
  }
  </>
   
  );
};
export default InfoBookSection;

const styles = StyleSheet.create({});
