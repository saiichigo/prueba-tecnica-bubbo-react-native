import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';

interface ButtonFloatProps {
  route: () => void
}

export const ButtonFloatAdd = ({route}: ButtonFloatProps) => {
  return (
    <View style={style.container}>
      <TouchableOpacity style={{marginBottom: 20, marginRight: 20}} onPress={route}>
      <Ionicons name="add-circle" size={60} color="green" />
      </TouchableOpacity>
    </View>
  );
};

const style = StyleSheet.create({
 container: {
    position: 'absolute',
    bottom: 1,
    right: 1,
    flexDirection: 'row', // Cambio a fila
    alignItems: 'flex-end', // 
 }
});
