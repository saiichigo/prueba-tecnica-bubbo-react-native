import React, {useState} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Alert,
  ImageBackground,
} from 'react-native';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {CameraOption} from '../interfaces/CameraOption';
import RNFS from 'react-native-fs';

interface Props {
  selectedImage: string;
  setSelectedImage: (selectedImage: string) => void;
  setSelectedImageBase64: (selectedImage: string) => void;
}

const ImageSelector = ({
  selectedImage,
  setSelectedImage,
  setSelectedImageBase64,
}: Props) => {
  const handleImagePick = () => {
    Alert.alert(
      'Seleccionar Imagen',
      'Elige una opción',
      [
        {
          text: 'Cámara',
          onPress: () => pickImage('camera'),
        },
        {
          text: 'Galería',
          onPress: () => pickImage('library'),
        },
        {
          text: 'Cancelar',
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  };

  const convertImageToBase64 = async (fileUri: string) => {
    try {
      const data = await RNFS.readFile(fileUri, 'base64');
      return data;
    } catch (error) {
      console.error(error);
    }
  };

  const pickImage = async (method: any) => {
    const options = {
      mediaType: 'photo',
      base64: true,
      quality: 1,
    };

    if (method === 'camera') {
      launchCamera(options, (response: CameraOption) => {
        if (!response.didCancel && !response.error) {
          const fileUri = response.assets[0].uri;
          convertImageToBase64(fileUri).then(base64String => {
            setSelectedImageBase64(base64String!);
          });
          setSelectedImage(fileUri);
        }
      });
    } else {
      launchImageLibrary(options, (response: CameraOption) => {
        if (!response.didCancel && !response.error) {
          const fileUri = response.assets[0].uri;
          convertImageToBase64(fileUri).then(base64String => {
            setSelectedImageBase64(base64String!);
          });
          setSelectedImage(fileUri);
        }
      });
    }
  };

  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'red',
        minHeight: 200,
      }}>
      <TouchableOpacity onPress={handleImagePick}>
        <ImageBackground
          source={
            selectedImage
              ? {uri: selectedImage}
              : require('../img/Image-not-found.png')
          }
          style={{width: 250, height: 200}}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </View>
  );
};

export default ImageSelector;
