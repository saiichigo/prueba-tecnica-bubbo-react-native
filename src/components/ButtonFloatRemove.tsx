import React from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import {StyleSheet, TouchableHighlight, View} from 'react-native';

interface ButtonFloatProps {
  deleteBook: any;
}

export const ButtonFloatRemove = ({deleteBook}: ButtonFloatProps) => {
  return (
    <View style={style.container}>
      <TouchableHighlight
        style={{marginRight: 10}}
        onPress={deleteBook}>
        <MaterialIcons name="delete" size={48} color="black" />
      </TouchableHighlight>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 20,
    right: 1,
    flexDirection: 'row', // Cambio a fila
    alignItems: 'flex-end', //
  },
});
