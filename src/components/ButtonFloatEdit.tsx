import React from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';

import {StyleSheet, TouchableOpacity, View} from 'react-native';

interface ButtonFloatProps {
  route: () => void;
}

export const ButtonFloatEdit = ({route}: ButtonFloatProps) => {
  return (
    <View style={style.container}>
      <TouchableOpacity
        style={{ marginRight: 10}}
        onPress={route}>
        <AntDesign name="edit" size={48} color="black" />
      </TouchableOpacity>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 80,
    right: 1,
    flexDirection: 'row', // Cambio a fila
    alignItems: 'flex-end', //

  },
});
