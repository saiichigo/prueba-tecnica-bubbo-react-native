import React from 'react';
import {Text, View} from 'react-native';

export const ErrorMessage = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text>Ha ocurrido en error al traer los datos</Text>
    </View>
  );
};
