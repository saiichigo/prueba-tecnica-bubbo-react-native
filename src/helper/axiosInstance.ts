import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';



interface DefaultHeaders {
    'Content-Type': string;
    Accept: string;
    'Access-Control-Allow-Origin': string;
}

const defaultHeaders: DefaultHeaders = {

    'Content-Type': 'application/json',
    // 'Content-Type': 'application/json',
    Accept: 'application/json',
    'Access-Control-Allow-Origin': '*',
};

const createInstance = (headers?: DefaultHeaders): AxiosInstance => {
    const instanceConfig: AxiosRequestConfig = {
        baseURL: 'https://render-test-7xaj.onrender.com',
        headers: { ...defaultHeaders, ...headers },
        // timeout: 10000000,
    };
    return axios.create(instanceConfig);
};

export const AxiosRequest = createInstance();