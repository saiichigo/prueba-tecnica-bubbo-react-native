import { AxiosRequest } from "../helper/axiosInstance";
import { BooksResponse } from "../interfaces/BooksResponse";

export const getBooks = async () => {
    try {
        const response = await AxiosRequest.get('/books');
        return response.data;


    } catch (error: any) {
        throw new Error('Error no esperado');
    }
};

export const getBookById = async (id: string) => {
    try {

        const response = await AxiosRequest.get(`/books/${id}`);
        return response.data;

    } catch (error) {
        throw new Error('Error no esperado');
    }

};
export const postNewBook = async (book: BooksResponse) => {
    try {

        const response = await AxiosRequest.post('/books', {
            book
        });
        return response.data;


    } catch (error) {
        console.log(error);

        throw new Error('Error el post');
    }



};
export const putBook = async (book: BooksResponse) => {
    const response = await AxiosRequest.put(`/books/${book.id}`, {
        book
    });
    return response.data;
};

export const deleteBoook = async (id: string) => {
    const response = await AxiosRequest.delete(`/books/${id}`);

    return response.data;
};

