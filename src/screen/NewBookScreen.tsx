import {View, ScrollView, TouchableOpacity, Text} from 'react-native';
import {Controller} from 'react-hook-form';
import {TextInput} from 'react-native-paper';
import ImageSelector from '../components/ImageSelector';
import {AirbnbRating} from 'react-native-ratings';
import DatePicker from 'react-native-date-picker';
import {Spinner} from '../components/Spinner';
import {useNewBooks} from '../hooks/useNewBooks';

const NewBookScreen = () => {
  const {
    isLoading,
    selectedImage,
    setSelectedImage,
    setSelectedImageBase64,
    control,
    errors,
    setOpen,
    open,
    date,
    setDate,
    handleSubmit,
    isValid,
    onSubmit,
    styles,
  } = useNewBooks();

  if (isLoading) {
    return <Spinner message="Procesando ..." />;
  }

  return (
    <View style={styles.container}>
      <View style={styles.imageSelectorContainer}>
        <ImageSelector
          selectedImage={selectedImage}
          setSelectedImage={setSelectedImage}
          setSelectedImageBase64={setSelectedImageBase64}
        />
      </View>
      <View style={styles.formContainer}>
        <ScrollView>
          <Controller
            name="title"
            control={control}
            rules={{required: true}}
            render={({field: {onChange, onBlur, value}}) => (
              <TextInput
                label="Titulo"
                onBlur={onBlur}
                mode="outlined"
                onChangeText={value => onChange(value)}
                value={value}
                style={styles.textInput}
              />
            )}
          />
          {errors.titulo && <Text>El titulo es requerido</Text>}

          <Controller
            name="publicationYear"
            control={control}
            rules={{required: true}}
            render={({field: {onChange, onBlur, value}}) => (
              <>
                <TextInput
                  label="Año de publicación"
                  onBlur={onBlur}
                  
                  // keyboardType="numeric"
                  maxLength={4}
                  mode="outlined"
                  onChangeText={value => onChange(value)}
                  value={value}
                  onFocus={() => setOpen(true)}
                  style={styles.textInput}
                />
                <DatePicker
                  modal
                  open={open}
                  date={date}
                  onConfirm={date => {
                    setOpen(false);
                    setDate(date);
                    onChange(date.toISOString().split('T')[0]); // Update the form state
                  }}
                  onCancel={() => {
                    setOpen(false);
                  }}
                />
              </>
            )}
          />

          {errors.publicationYear && <Text>Debes ingresar un fecha</Text>}

          <Controller
            name="cost"
            control={control}
            rules={{required: true}}
            render={({field: {onChange, onBlur, value}}) => (
              <TextInput
                label="Precio"
                onBlur={onBlur}
                mode="outlined"
                onChangeText={value => onChange(value)}
                value={value}
                style={styles.textInput}
              />
            )}
          />
          {errors.cost && <Text>Debes ingresar un monto del libro</Text>}
          <Controller
            name="author"
            control={control}
            rules={{required: true}}
            render={({field: {onChange, onBlur, value}}) => (
              <TextInput
                label="Autor"
                onBlur={onBlur}
                mode="outlined"
                onChangeText={value => onChange(value)}
                value={value}
                style={styles.textInput}
              />
            )}
          />
          {errors.author && <Text>Debes agregar el nombre del Autor</Text>}
          <Controller
            name="pages"
            control={control}
            rules={{required: true}}
            render={({field: {onChange, onBlur, value}}) => (
              <TextInput
                label="Paginas del libro"
                keyboardType="numeric"
                maxLength={4}
                onBlur={onBlur}
                mode="outlined"
                onChangeText={value => onChange(value)}
                value={value}
                style={styles.textInput}
              />
            )}
          />
          {errors.pages && <Text>Debes agregar el numero de paginas</Text>}

          <Controller
            name="genre"
            control={control}
            rules={{required: true}}
            render={({field: {onChange, onBlur, value}}) => (
              <TextInput
                label="Genero"
                onBlur={onBlur}
                mode="outlined"
                onChangeText={value => onChange(value)}
                value={value}
                style={styles.textInput}
              />
            )}
          />
          {errors.genre && <Text>Debes agregar el genero del libro</Text>}

          <Controller
            name="description"
            control={control}
            rules={{required: true}}
            render={({field: {onChange, onBlur, value}}) => (
              <TextInput
                label="Descripción"
                onBlur={onBlur}
                mode="outlined"
                onChangeText={value => onChange(value)}
                value={value}
                multiline // Habilita múltiples líneas
                maxLength={500} // Establece el número máximo de caracteres, ajusta según necesites
                style={styles.textInput}
              />
            )}
          />
          {errors.description && (
            <Text>Debes hacer una breve descripción del libro</Text>
          )}
          <Controller
            name="rate"
            control={control}
            rules={{required: true}}
            render={({field: {onChange, onBlur, value}}) => (
              <>
                <Text style={styles.rateText}>Agrega una calificación</Text>
                <AirbnbRating
                  count={5}
                  reviews={['Terrible', 'Mala', 'Normal', 'Buena', 'Excelente']}
                  defaultRating={0}
                  size={20}
                  onFinishRating={rating => onChange(rating)}
                  reviewSize={20}
                />
              </>
            )}
          />

          <TouchableOpacity
            onPress={handleSubmit(onSubmit)}
            disabled={!isValid && selectedImage === ''}
            style={[
              styles.submitButton,
              {
                backgroundColor: isValid ? 'green' : 'grey',
              },
            ]}>
            <View style={{alignItems: 'center'}}>
              <Text style={styles.submitButtonText}>Guardar</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </View>
  );
};

export default NewBookScreen;
