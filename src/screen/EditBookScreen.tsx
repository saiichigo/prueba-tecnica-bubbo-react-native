import { View, ScrollView, TouchableOpacity, Text } from 'react-native';
import { useEffect } from 'react';
import { Controller } from 'react-hook-form';
import { TextInput } from 'react-native-paper';
import ImageSelector from '../components/ImageSelector';
import { AirbnbRating } from 'react-native-ratings';
import DatePicker from 'react-native-date-picker';
import {Spinner} from '../components/Spinner';
import {useEditBook} from '../hooks/useEditBook';

const EditBookScreen = () => {
  const {
    data,
    isLoading,
    control,
    handleSubmit,
    date,
    setDate,
    open,
    setOpen,
    setSelectedImage,
    setSelectedImageBase64,
    isLoadingUpdate,
    errors,
    onSubmit,
    isValid,
    styles,
  } = useEditBook();

  useEffect(() => {
    if (data) {
      console.log(data.cost);

      const initialData = {
        images: data.images,
        title: data.title,
        publicationYear: data.publicationYear,
        cost: String(data.cost),
        author: data.author,
        pages: String(data.pages),
        genre: data.genre,

        description: data.description,
        rate: data.rate,
      };
      Object.keys(initialData).forEach(key => {
        //@ts-ignore
        setValue(key, initialData[key]);
      });
    }
  }, [data]);

  return (
    <View style={styles.container}>
      {isLoading || isLoadingUpdate ? (
        <Spinner message={isLoading ? 'Cargando info' : 'Actualizando...'} />
      ) : (
        <>
          <View style={styles.imageSelectorContainer}>
            <Controller
              name="images"
              control={control}
              rules={{required: true}}
              render={({field: {onChange, onBlur, value}}) => (
                <ImageSelector
                  selectedImage={value}
                  setSelectedImage={setSelectedImage}
                  setSelectedImageBase64={setSelectedImageBase64}
                />
              )}
            />
          </View>
          <View style={styles.formContainer}>
            <ScrollView>
              <Controller
                name="title"
                control={control}
                rules={{required: true}}
                render={({field: {onChange, onBlur, value}}) => (
                  <TextInput
                    label="Titulo"
                    onBlur={onBlur}
                    mode="outlined"
                    onChangeText={value => onChange(value)}
                    value={value}
                    style={styles.textInput}
                  />
                )}
              />
              {errors.titulo && <Text>El titulo es requerido</Text>}

              <Controller
                name="publicationYear"
                control={control}
                rules={{required: true}}
                render={({field: {onChange, onBlur, value}}) => (
                  <>
                    <TextInput
                      label="Año de publicación"
                      onBlur={onBlur}
                      mode="outlined"
                      onChangeText={value => onChange(value)}
                      value={value}
                      onFocus={() => setOpen(true)}
                      style={styles.textInput}
                    />
                    <DatePicker
                      modal
                      open={open}
                      date={date}
                      onConfirm={date => {
                        setOpen(false);
                        setDate(date);
                        onChange(date.toISOString().split('T')[0]); // Update the form state
                      }}
                      onCancel={() => {
                        setOpen(false);
                      }}
                    />
                  </>
                )}
              />

              {errors.publicationYear && <Text>Debes ingresar un fecha</Text>}

              <Controller
                name="cost"
                control={control}
                rules={{required: true}}
                render={({field: {onChange, onBlur, value}}) => (
                  <TextInput
                    label="Precio"
                    onBlur={onBlur}
                    mode="outlined"
                    onChangeText={value => onChange(value)}
                    value={value}
                    style={styles.textInput}
                  />
                )}
              />
              {/* {errors.cost && <Text>Debes ingresar un monto del libro</Text>} */}
              <Controller
                name="author"
                control={control}
                rules={{required: true}}
                render={({field: {onChange, onBlur, value}}) => (
                  <TextInput
                    label="Autor"
                    onBlur={onBlur}
                    mode="outlined"
                    onChangeText={value => onChange(value)}
                    value={value}
                    style={styles.textInput}
                  />
                )}
              />

              {errors.author && <Text>Debes agregar el nombre del Autor</Text>}

              <Controller
                name="pages"
                control={control}
                rules={{required: true, maxLength: 4}}
                render={({field: {onChange, onBlur, value}}) => (
                  <TextInput
                    label="Paginas del libro"
                    keyboardType="numeric"
                    maxLength={4}
                    onBlur={onBlur}
                    mode="outlined"
                    onChangeText={value => onChange(value)}
                    value={value}
                    style={styles.textInput}
                  />
                )}
              />

              {errors.pages && <Text>Debes agregar el numero de paginas</Text>}

              <Controller
                name="genre"
                control={control}
                rules={{required: true}}
                render={({field: {onChange, onBlur, value}}) => (
                  <TextInput
                    label="Genero"
                    onBlur={onBlur}
                    mode="outlined"
                    onChangeText={value => onChange(value)}
                    value={value}
                    style={styles.textInput}
                  />
                )}
              />
              {errors.genre && <Text>Debes agregar el genero del libro</Text>}

              <Controller
                name="description"
                control={control}
                rules={{required: true}}
                render={({field: {onChange, onBlur, value}}) => (
                  <TextInput
                    label="Descripción"
                    onBlur={onBlur}
                    mode="outlined"
                    onChangeText={value => onChange(value)}
                    value={value}
                    multiline // Habilita múltiples líneas
                    maxLength={500} // Establece el número máximo de caracteres, ajusta según necesites
                    style={styles.textInput}
                  />
                )}
              />
              {errors.description && (
                <Text>Debes hacer una breve descripción del libro</Text>
              )}
              <Controller
                name="rate"
                control={control}
                rules={{required: true}}
                render={({field: {onChange, onBlur, value}}) => (
                  <>
                    <Text style={styles.rateText}>Calificación</Text>
                    <AirbnbRating
                      count={5}
                      reviews={[
                        'Terrible',
                        'Mala',
                        'Normal',
                        'Buena',
                        'Excelente',
                      ]}
                      defaultRating={value}
                      size={20}
                      onFinishRating={rating => onChange(rating)}
                      reviewSize={20}
                    />
                  </>
                )}
              />

              <TouchableOpacity
                onPress={handleSubmit(onSubmit)}
                disabled={!isValid}
                style={[
                  styles.submitButton,
                  {
                    backgroundColor: isValid ? 'green' : 'grey',
                  },
                ]}>
                <View style={{alignItems: 'center'}}>
                  <Text style={styles.submitButtonText}>Guardar cambios</Text>
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </>
      )}
    </View>
  );
};

export default EditBookScreen;
