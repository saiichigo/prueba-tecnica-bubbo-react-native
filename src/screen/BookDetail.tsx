import { Image, ScrollView, Text, View } from 'react-native';
import {Spinner} from '../components/Spinner';
import {ErrorMessage} from '../components/ErrorMessage';
import {ButtonFloatEdit} from '../components/ButtonFloatEdit';
import {ButtonFloatRemove} from '../components/ButtonFloatRemove';
import {useDetailBook} from '../hooks/useDetailBook';
import InfoBookSection from '../components/InfoBookSection';

const BookDetail = () => {
  const {
    isLoadingDelete,
    error,
    isLoading,
    data,
    _deleteBook,
    redirectScreen,
    styles,
  } = useDetailBook();

  if (isLoadingDelete) {
    return <Spinner message="Eliminando...." />;
  }
  if (isLoading) {
    return <Spinner message="cargando...." />;
  }

  if (error) {
    return <ErrorMessage />;
  }

  return (
    <View style={styles.container}>
      <View style={styles.imageSection}>
        <Image
          source={{uri: data?.images}}
          style={styles.imageBackground}
          resizeMode={'contain'}
        />
      </View>
      <View style={styles.detailsSection}>
        <Text style={styles.titleTitle}>{data?.title}</Text>
        <Text style={styles.titleAuthor}>by {data?.author}</Text>
        <Text style={styles.titleYear}>{data?.publicationYear}</Text>

        <View style={styles.infoBox}>
         
         <InfoBookSection data={data?.rate!} title='Rate'/>
         <InfoBookSection data={data?.cost!} title='Costo'/>
         <InfoBookSection data={data?.pages!} title='Paginas' disableLine/>
        </View>
      </View>
      <View style={styles.descriptionSection}>
        <ScrollView>
          <Text style={styles.descriptionText}>Descripción</Text>
          <Text style={styles.titleOverview}>{data?.description}</Text>
        </ScrollView>
      </View>
      <ButtonFloatRemove deleteBook={_deleteBook} />
      <ButtonFloatEdit route={() => redirectScreen('edit')} />
    </View>
  );
};

export default BookDetail;
