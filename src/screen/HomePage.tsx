import {
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {useGlobal} from '../hooks/useGlobal';
import {Spinner} from '../components/Spinner';
import {ErrorMessage} from '../components/ErrorMessage';
import {ButtonFloatAdd} from '../components/ButtonFloatAdd';
import {AirbnbRating} from 'react-native-ratings';

const HomePage = () => {
  const {data, error, isLoading, redirectScreen, styles} = useGlobal();

  if (isLoading) {
    return <Spinner message="Cargando información..." />;
  }

  if (error) {
    return <ErrorMessage />;
  }

  const renderItemBook = ({item}: any) => {
  

    return (
      <TouchableOpacity onPress={() => redirectScreen('bookDetail', item.id)}>
        <View style={styles.itemContainer}>
          <View style={styles.sectionleft}>
            <Image
              source={{uri: item.images}}
              style={styles.image}
              // resizeMode={'contain'}
            />
          </View>
          <View style={styles.sectionRight}>
            <Text style={styles.textTitle}>{item.title}</Text>
            <Text style={styles.textAuthor}>{item.author}</Text>
            <AirbnbRating
              count={5}
              reviews={[]}
              defaultRating={item.rate}
              size={20}
              starContainerStyle={{
                // top: -40,
              }}
              showRating={false}
              // reviewSize={5}
            />
            <Text style={styles.textAuthor}>{`$${item.cost}`}</Text>

          </View>
        </View>

        <View style={{height: 1, backgroundColor: 'grey', width: '100%'}} />


      </TouchableOpacity>
    );
  };

  return (
    <View style={{flex: 1}}>
      <FlatList
        data={data}
        renderItem={renderItemBook}
        keyExtractor={(item, index) => index.toString()}
        // showsHorizontalScrollIndicator={false}
      />

      <ButtonFloatAdd route={() => redirectScreen('newBook', 'asdasd')} />
    </View>
  );
};



export default HomePage;
