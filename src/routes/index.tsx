import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomePage from '../screen/HomePage';
import BookDetail from '../screen/BookDetail';
import NewBookScreen from '../screen/NewBookScreen';
import EditBookScreen from '../screen/EditBookScreen';

const Stack = createNativeStackNavigator();

function RootNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="home">
        <Stack.Screen
          name="home"
          component={HomePage}
          options={{
            title: 'Pantalla principal',
            headerTitleAlign: 'center',
          }}
        />
        <Stack.Screen
          name="bookDetail"
          component={BookDetail}
          options={{
            title: 'Detalle Libro',
            headerTitleAlign: 'center',
          }}
        />
        <Stack.Screen
          name="newBook"
          component={NewBookScreen}
          options={{
            title: 'Agregar Libro',
            headerTitleAlign: 'center',
          }}
        />
        <Stack.Screen
          name="edit"
          component={EditBookScreen}
          options={{
            title: 'Editar Libro',
            headerTitleAlign: 'center',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default RootNavigation;
